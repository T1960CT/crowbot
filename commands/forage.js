const { MessageAttachment } = require("discord.js");

module.exports = {
  name: "forage",
  description: "forage",
  execute(message, args) {
    let rootArg = "";
    if (args.length > 0) rootArg = args[0];

    content = getList();
    if (content == "") {
      message.channel.send("No data!");
      return;
    }

    // At some point this will need to take into account other countries
    if (rootArg == "random") {
      strong = fetchLinks(makeArray(getRandomForage(content)));
      postMessage(message, [{ name: "__Random!__", value: strong }]);
      return;
    }

    // strong = fetchLinks(content.strong_harvest)
    strong = content.strong_harvest;
    // possible = fetchLinks(content.possible_harvest)
    possible = content.possible_harvest;

    generateImage(content);

    try {
      const img = fs.readFileSync("/tmp/output.png");
      const attachment = new MessageAttachment(img);
      message.channel.send(attachment);
    } catch (e) {
      console.error(e);
    }
  },
};

const { exec } = require("child_process");
function generateImage(contents) {
  // Program src: https://gitlab.com/T1960CT/text2image
  let header = [
    "Range: " + content.range,
    "",
    "Region: North America",
    "",
    "Sources:",
    "- Forager's Harvest",
    "- Nature's Garden",
    "- Incredible Wild Edibles",
    "by: Sam Thayer",
  ];
  let strong = ["", "", "STRONG HARVEST", ""];
  let possible = ["", "", "POSSIBLE HARVEST", ""];

  strong.push(...contents.strong_harvest);
  possible.push(...contents.possible_harvest);

  let lines = [];
  lines.push(...header);
  lines.push(...strong);
  lines.push(...possible);

  exec(
    "./text2image -o=/tmp/output.png -s=2 " +
      lines.map((i) => `"${i}"`).join(" "),
    (error, stdout, stderr) => {
      console.log(error, stdout, stderr);
    }
  );
}

function postMessage(message, fields) {
  message.channel.send({
    embed: {
      title: "",
      description: "",
      color: "#23d160",
      fields: fields,
    },
  });
}

function makeArray(contents) {
  return [contents];
}

const fs = require("fs");

function getList() {
  var today = new Date();
  var dd = String(today.getDate());
  var months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  var mm = months[today.getMonth()];
  var foraging = JSON.parse(fs.readFileSync("foraging.json", "utf8"));

  switch (mm) {
    case "January":
      return foraging.mid_winter;
    case "February":
      if (dd <= 14) return foraging.mid_winter;
      return foraging.late_winter;
    case "March":
      if (dd <= 24) return foraging.late_winter;
      return foraging.early_spring;
    case "April":
      if (dd <= 25) return foraging.early_spring;
      return foraging.mid_spring;
    case "May":
      if (dd <= 10) return foraging.mid_spring;
      return foraging.late_spring;
    case "June":
      if (dd <= 5) return foraging.late_spring;
      return foraging.early_summer;
    case "July":
      return foraging.mid_summer;
    case "August":
      if (dd <= 10) return foraging.mid_summer;
      return foraging.late_summer;
    case "September":
      if (dd >= 10) return foraging.early_fall;
      return foraging.late_summer;
    case "October":
      if (dd >= 10) return foraging.late_fall;
      return foraging.early_fall;
    case "November":
      if (dd <= 15) return foraging.late_fall;
      return foraging.early_winter;
    case "December":
      return foraging.early_winter;
    default:
      return "";
  }
}

function getRandomForage(contents) {
  return contents.strong_harvest[
    Math.floor(Math.random() * contents.strong_harvest.length)
  ];
}

function fetchLinks(contents) {
  const links = JSON.parse(fs.readFileSync("foraging_index.json", "utf8"));
  var content = "";

  for (c of contents) {
    content += `[${c}](${links.wiki_links[c]})\n`;
  }
  return content;
}

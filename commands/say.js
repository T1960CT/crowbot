module.exports = {
    name: 'say',
    description: 'Make the bot say something',
    execute(message, args, client) {
        if (message.member.hasPermission('ADMINISTRATOR')) {
            const chan = client.channels.cache.find(channel => channel.name === args[0])
            let channame = args[0]
            delete (args[0])
            // Find which server the command was invoked in
            client.guilds.cache.get(message.channel.guild.id)
                // Search that server for a channel with the name provided in args[0]
                .channels.cache.find(channel => channel.name === channame)
                // Send it the contents provided
                .send(args.join(' '))
        }
    }
}

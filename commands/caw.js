module.exports = {
  name: 'caw',
  description: 'caw',
  execute (message, args, avatar) {
    message.channel.send({
      embed: {
        color: '#23d160',
        title: `**C̗̫̗̙͈͕ͅA͙̺̳͕ͪͨͫ̂W͙̹̦͉ͤ̉̍̾ C͊͂̍̌͒̒ͬA͇̮͖̞̝̯ͭ͌ͤ͊͆̈́W̰̜͒͗ ̨̙̰͈͕̱͙C̍͛̋ͧĄ͎̲̳̘̘͙̓̑̍̓̽̑̚ͅW̝̠ ̜ͯC̊ͫ̍͛ͩ̀Ḁ̸̪̭̯̳͗͂̽̇̓͑ͅẄ̖̻́ͦ\n C̷̢͓͔͔͓̞͖̦̩͍̘̝̪̣̦̥̙̰͉̿̍̏̊Ą̵̢̧̡̛̛̺̺̪̹̠̣͉̙͚̲̗̙͖̹͚̪̖̮͇͎̬̙̗͙̖͓͖̰̟̬̦̘͈͉͕͙̟̇̀͊͆̊̔̃̐̔̿̐̓͛̇̿̽̾̑̉̈́͊̆̀̔̊̒͗̾͋̐̀͘͘͝W̷̨̧̨̛̬̗̱͙̹̮͍̘͔̘̺̳̬̮̄̋̑̋͂͒͋̇̋́̽̂̅͛̊̈́͌̒͛́̂̓̌͗̕͘̕͘͝͝͠ ̵̮̈́̆͗̀̽̓̇͊͂̏̄̇̓̓̿͑̓͐̅̈̐͆̏͆̊̇͐͗̈́͆̏̅̋̚͝͝͝͝͠͠C̶̛̤̳̬͙̮̄́͛̑̽̀̋̎͘͠Ȃ̶̡̛͈̳͇͖͓̪̟͎̥͕̼͖͓̩̗͕̥̪͔̬̤̞̩̺̀̃̅̾̀̌̀̓̏͌̓̍́̽̔̊͜ͅW̴̢̢͉͎̣̤͙͚̬̖̪̙̘̥̲̣̲̞̣͚̣̝͇̝̠̦̬̼̱͈̘̍̈̽͒̈́̔̍̋̍͛͐͆̀̽̏͜͝͝ ̷̡̨̢̢̩͉̮͇̟̹̙͉̲̱̰͎͈̗͖̞̗̗͉̹̺̻̗̩̮̯̗̦̐ͅͅC̷̬̥͕͑̂̑̑̄̽̅̏̇̑̾͛̆̇́̓̄̑̅̓͐̍̽͋̒̎͋̈̃͛̕͝͠͠A̵̛̭͎̫̦̣̖̪͔̘͛́̌̔̂̓̓̿̌̍͂́̅̏̽̕̕͜͠͠W̴̛̗̮̭̪͙̃̉̍͑̓̿̂͂̈́̃̓̈́͌́̄̽̆̒̇̿̇͜͝ͅ ̶̧̧͈̯̤̣̹̙͕͉͓̟̟̬̳̻͔̖͙̺̹͓̤̠̪̻̮̫̝̐̓͊̔́̌̋̔̓̏̂̀̑̑́̉̾̈́̓̀͋͗̓̀̃͌̌͜͜͝͝͝͝͠͠C̴̛̼̬̣̻̝̎̃̓͒̑̿̀͂̿̾͐͑͊͋̏̽̔̔̔̄̃͑̌̔̅̅̈́͗͒̈́̄̀͘͠͠A̵̧̧̤͔͉͕̺͎̯̼̭̺̫͇̖̟̩̜͔͚̼̻͓̠͕̳̭̱̥̳͖̹͈̯̝̖̻͈̫̮̓̂̂̊̒̓̂̾́͗̌̊̔͗́͂̔̈́̂̏͐͐̉̈́̅͑͜͠ͅW̶̨͈̻̲̩̆̎͂̃̆̄́̔͑͐̇͒̇̽̍̏́̑̇͌̒̓̕͘͘͝\nC̗̫̗̙͈͕ͅA͙̺̳͕ͪͨͫ̂W͙̹̦͉ͤ̉̍̾ C͊͂̍̌͒̒ͬA͇̮͖̞̝̯ͭ͌ͤ͊͆̈́W̰̜͒͗ ̨̙̰͈͕̱͙C̍͛̋ͧĄ͎̲̳̘̘͙̓̑̍̓̽̑̚ͅW̝̠ ̜ͯC̊ͫ̍͛ͩ̀Ḁ̸̪̭̯̳͗͂̽̇̓͑ͅẄ̖̻́ͦ**`,
        thumbnail: {
          url: avatar
        }
      }
    })
  }
}

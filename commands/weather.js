const request = require('request')

module.exports = {
  name: 'weather',
  description: 'The Weather',
  execute(message, args) {
    let loc = (args[0] == "short") ? args[1] : args[0]
    if (loc == undefined) loc = 'ottawa'

    request.get(`http://wttr.in/${loc}?format=j1`, (error, resp, body) => {
      jsonBody = JSON.parse(body)

      if (jsonBody['nearest_area'][0]['areaName'][0]['value'] == "Not") {
        message.channel.send('Area not found: `' + loc + '`')
        return
      }

      tempC = jsonBody['current_condition'][0]['temp_C']
      tempFeels = jsonBody['current_condition'][0]['FeelsLikeC']
      tempMax = jsonBody['weather'][0]['maxtempC']
      tempMin = jsonBody['weather'][0]['mintempC']
      uvIndex = jsonBody['weather'][0]['uvIndex']

      humidity = jsonBody['current_condition'][0]['humidity']
      precipitation = jsonBody['current_condition'][0]['precipMM']
      snowfallCM = jsonBody['weather'][0]['totalSnow_cm']

      sky = jsonBody['current_condition'][0]['weatherDesc'][0]['value']
      cover = jsonBody['current_condition'][0]['cloudcover']

      pressure = jsonBody['current_condition'][0]['pressure']
      visibility = jsonBody['current_condition'][0]['visibility']

      windSpeed = jsonBody['current_condition'][0]['windspeedKmph']
      windDegrees = jsonBody['current_condition'][0]['winddirDegree']
      windDir = jsonBody['current_condition'][0]['winddir16Point']

      observedTime = jsonBody['current_condition'][0]['localObsDateTime']

      sunrise = jsonBody['weather'][0]['astronomy'][0]['sunrise']
      sunset = jsonBody['weather'][0]['astronomy'][0]['sunset']
      sunhours = jsonBody['weather'][0]['sunHour']
      moonphase = jsonBody['weather'][0]['astronomy'][0]['moon_phase']

      fields = []
      if (args[0] == "short" && args[1] != "") {
        fields = [
          {
            name: 'Report',
            value: `**Hi**: ${tempMax}°C / **Lo**: ${tempMin}°C\n**Current**: ${tempC}°C\n**Feels Like**: ${tempFeels}°C\n**Humidity**: ${humidity}%`
          }
        ]
      } else if (args[0] != "") {
        fields = [
          {
            name: '__Temperature__',
            value: `**Hi**: ${tempMax}°C / **Lo**: ${tempMin}°C\n**Current**: ${tempC}°C\n**Feels Like**: ${tempFeels}°C\n**UV Index**: ${uvIndex}`
          },
          {
            name: '__Moisture__',
            value: `**Humidity**: ${humidity}%\n**Precipitation**: ${precipitation}mm\n**Snowfall**: ${snowfallCM}cm`
          },
          {
            name: '__Sky__',
            value: `**Conditions**: ${sky}\n**Cloud Coverage**: ${cover}%`
          },
          {
            name: '__Atmospheric__',
            value: `**Pressure**: ${pressure} kPa\n**Visibility**: ${visibility} KM`
          },
          {
            name: '__Wind__',
            value: `**Speed**: ${windSpeed} KM/h\n**Direction**: ${windDegrees}°${windDir}`
          },
          {
            name: '__Astronomic__',
            value: `**Sun Rise**: ${sunrise}\n**Sun Set**: ${sunset}\n**Sun Hours**: ${sunhours} Hours\n**Moon Phase**: ${moonphase}`
          }
        ]
      } else if (args[0] == "") {
        message.channel.send('You have to specify a city')
        return
      } else {
        message.channel.send('Something went wrong')
        return
      }

      message.channel.send({
        content: "Here's your stupid weather",
        embed: {
          color: '#23d160',
          title: loc.charAt(0).toUpperCase() + loc.slice(1) + ' Weather Report',
          fields: fields,
          footer: {
            text: `Observed At: \u00A0 ${observedTime}`
          }
        }
      })
    })
  }
}

const responses = [
  "You're a walking 'before' picture.",
  "You didn't just fall out of the stupid tree. You were dragged through dumbass forest.",
  'You are a disappointment to your parents.',
  "Shut up, you'll never be the man your mother is.",
  "You're so dumb it hurts.",
  "I don't _exactly_ hate you, but if you were on fire and I had water, I'd drink it.",
  "A chain is only as strong as it's weakest link. Hello weakest link.",
  'You have something on your chin. No, the other one.',
  'You should never play hide and seek. No one would look for you.',
  "I'm busy. Can't I ignore you some other time?",
  "You're like a Monday morning, nobody likes you."
]

module.exports = {
  name: 'insult',
  description: 'Insult Moods',
  execute (message, args) {
    message.channel.send(
      responses[Math.floor(Math.random() * responses.length - 1)]
    )
  }
}

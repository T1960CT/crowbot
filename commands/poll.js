const fs = require('fs')
var emojis = JSON.parse(fs.readFileSync('emojis.json', 'utf8'))

module.exports = {
  name: 'poll',
  description: 'poll',
  execute (message, args, client) {
    aj = args.join(' ')
    if (aj != '') {
      // Default yes/no/maybe poll
      if (!aj.includes(',')) {
        message
          .react(emoji('white_check_box'))
          .then(() => message.react(emoji('x')))
          .then(() => message.react(emoji('grey_question')))
        return
      }

      // Generate multiple choice poll
      multi = aj.split(';')
      try{
        multipleChoice = multi[1].replace('?', '').split(',')
      message.channel
        .send({
          embed: {
            color: '#23d160',
            title: 'Weigh in!',
            description: multi[0],
            fields: {
              name: 'Options',
              value: generatePollMultiple(multipleChoice)
            }
          }
        })
        .then(async function (message) {
          emojisList = pollEmojiList(multipleChoice.length)
          for (e of emojisList) {
            await message.react(emoji(e))
          }
        })
      } catch {
        message.channel
        .send({
          embed: {
            color: colour,
            description:
              'Poll sets a yes/no/maybe poll, or with some context can make multiple choice polls.',
            fields: [
              {
                name: 'Command(s)',
                value: '`!poll [question]\n!poll [question]?; [choice1], [choice2], [choice3]`'
              }
            ],
            footer: footer
          }
        })
      }
    }
  }
}

function emoji (codepoint) {
  return String.fromCodePoint(parseInt(`0x${emojis[codepoint]}`))
}

function generatePollMultiple (options) {
  list = pollEmojiList(options.length)

  // TODO: why is the first one not being capitalized? Figure out this indexing

  index = 0
  valString = ''
  for (e of list) {
    capitalized =
      options[index].charAt(0).toUpperCase() + options[index].slice(1)
    valString += emoji(e) + ` = ${capitalized}\n`
    index++
  }
  return valString
}

function pollEmojiList (length) {
  list = new Array()
  char = 97 + length
  for (i = 97; i < char; i++) {
    list.push(`regional_indicator_${String.fromCharCode(i)}`)
  }
  return list
}

const commands = {
  caw,
  crowball,
  weather,
  poll,
  forage,
  '': generateDefault
}

module.exports = {
  name: 'help',
  description: 'help',
  execute (message, args, avatar) {
    aj = args.join(' ')
    message.channel.send(commands[aj](avatar))
  }
}

const colour = '#23d160'
const footer = { text: `CrowBot written by: amgry crow` }

function generateDefault (avatar) {
  return {
    embed: {
      color: colour,
      title: 'Help Command List',
      thumbnail: {
        url: avatar
      },
      fields: [
        {
          name: '__Just for Fun__',
          value: '`!help caw`\n`!help crowball`',
          inline: true
        },
        {
          name: '__Utilities__',
          value: '`!help weather`\n`!help poll\n!help forage`',
          inline: true
        },
        {
          name: '__Not Helping?__',
          value: 'Post a message in CrowBot\'s\n\`general\` or \`bugs-issues\` channels!',
          inline: false
        }
      ],
      footer: footer
    }
  }
}

function caw () {
  return {
    embed: {
      color: colour,
      description: 'Caw triggers the murder.',
      fields: [
        {
          name: 'Command',
          value: '`!caw`'
        }
      ],
      footer: footer
    }
  }
}

function crowball () {
  return {
    embed: {
      color: colour,
      description: 'Crow Ball is a magic 8 ball.',
      fields: [
        {
          name: 'Command',
          value: '`!crowball [question]`'
        }
      ],
      footer: footer
    }
  }
}

function weather () {
  return {
    embed: {
      color: colour,
      description: 'Fetches the weather report for the specified city.',
      fields: [
        {
          name: 'Commands',
          value: '`!weather [city]`\n`!weather short [city]`'
        }
      ],
      footer: footer
    }
  }
}

function poll () {
  return {
    embed: {
      color: colour,
      description:
        'Poll sets a yes/no/maybe poll, or with some context can make multiple choice polls.',
      fields: [
        {
          name: 'Command(s)',
          value: '`!poll [question]\n!poll [question]?; [choice1], [choice2], [choice3]`'
        }
      ],
      footer: footer
    }
  }
}

function forage () {
  return {
    embed: {
      color: colour,
      description:
        'Forage shows foragable items listed in two groups:\nStrong Forage (very likely to find)\nPossible Harvest (going out of, or coming into season)\n_Note: Only applies to North America._',
      fields: [
        {
          name: 'Command(s)',
          value: '`!forage`\n`!forage random`'
        }
      ],
      footer: footer
    }
  }
}

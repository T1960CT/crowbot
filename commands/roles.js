const { roles } = require('./db.js')

const groupCommands = {
  add,
  remove,
  list,
  display
}

module.exports = {
  name: 'role',
  description: 'Reaction Roles',
  async execute (message, args, client) {
    // Sample
    // !role add PRIMARY @happy :smile:
    // !role remove PRIMARY @happy
    // !role list PRIMARY
    // !role display

    const [command, group, role, reaction] = args
    let sts = await groupCommands[command](group, role, reaction)
    message.channel.send(sts).then(embed => {
      if (command == 'display') {
        for (reactions of sts['embed'].fields) {
          embed.react(reactions.name)
        }
      }
    })
  }
}

const sampleRole = [
  { group: 'primary', role: 'happy', emote: ':happy:' },
  { group: 'primary', role: 'sad', emote: ':sad:' }
]

async function add (group, role, reaction) {
  return roles
    .insert({ group, role, reaction })
    .then(() => {
      return 'Added '
    })
    .catch(err => {
      return err
    })
}

async function remove (group, role) {
  return roles
    .remove({ group, role })
    .then(() => {
      return 'Removed'
    })
    .catch(err => {
      return err
    })
}

async function list (group) {
  return roles
    .find()
    .then(res => {
      if (group == undefined) {
        return `Groups: ` + res.map(e => e.group).join(', ')
      }

      res.filter(r => r.group == group)
      if (res.length == 0) {
        return 'No Roles in this Group'
      }
      return (
        `Roles in \`${group}\`:\n` +
        res.map(e => e.reaction + ' ' + e.role).join(',\n')
      )
    })
    .catch(err => {
      return err
    })
}

async function display (group) {
  return roles.find().then(res => {
    if (group == undefined) {
      return `Global display not supported yet, please specify group name`
    }

    res.filter(r => r.group == group)
    if (res.length == 0) {
      return 'No Roles in this Group'
    }

    const group2 = group.charAt(0).toUpperCase() + group.slice(1)
    const fields = res.map(function (e) {
      return { name: e.reaction, value: e.role }
    })

    return {
      embed: {
        color: '#23d160',
        title: 'Roles - ' + group2,
        fields
      }
    }
  })
}

function toggleRole (args, client) {}

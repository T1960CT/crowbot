const path = require('path')
var Datastore = require('nedb-promises')

const users = new Datastore({
  filename: path.join('data', 'users.db'),
  autoload: true
})

const roles = new Datastore({
  filename: path.join('data', 'roles.db'),
  autoload: true
})

module.exports = { users, roles }

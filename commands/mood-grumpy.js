const responses = [
  'Leave me alone, damnit.',
  "I'm not in the mood for this.",
  "Roses are red, violets are blue. I have 5 fingers, the 3rd one's for you. :middle_finger:",
  'Fuck off, already.',
  "Why don't you slip into something more comfortable -- like a coma.",
  'Knock knock. Leave.',
  'How many times do I have to flush to get rid of you?',
  'You were adopted.',
  'What do you want, accident?',
  'You only annoy me when you breathe.',
  "Please cancel my subscription to whatever you're talking about.",
  '_grumble grumble_'
]

module.exports = {
  name: 'grumpy',
  description: 'Grumpy Moods',
  execute (message, args) {
    message.channel.send(
      responses[Math.floor(Math.random() * responses.length - 1)]
    )
  }
}

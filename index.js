const Discord = require('discord.js')

const fs = require('fs')
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'))

const prefix = '!'

const client = new Discord.Client()
client.login(config['apiKey'])
client.once('ready', () => {
  console.log('CrowBot up and running')
  client.user.setActivity('you sleep', { type: 'WATCHING' })
})

client.commands = new Discord.Collection()

const commands = fs
  .readdirSync('./commands')
  .filter(file => file.endsWith('.js'))
for (const file of commands) {
  const command = require(`./commands/${file}`)
  client.commands.set(command.name, command)
}

client.on('guildMemberAdd', member => {
  member.guild.channels.cache
    .get(member.guild.channels.cache.find(channel => channel.name === "general").id)
    .send("Welcome, " + member.displayName + "!")
})
client.on('guildMemberRemove', member => {
  member.guild.channels.cache
    .get(member.guild.channels.cache.find(channel => channel.name === "general").id)
    .send("Bye, " + member.displayName + "!")
})

client.on('message', message => {
  // If a bot sent the message, ignore it
  if (message.author.bot) return

  // If the message starts with command prefix '!'
  if (message.content.startsWith(prefix)) {
    const args = message.content.slice(prefix.length).split(/ +/)
    const command = args.shift().toLowerCase()
    avatar = client.user.avatarURL()

    // Refactor this garbage
    switch (command) {
      case 'caw':
        client.commands.get('caw').execute(message, args, avatar)
        return
      case 'crowball':
        client.commands.get('8ball').execute(message, args)
        return
      case 'weather':
        client.commands.get('weather').execute(message, args)
        return
      case 'forage':
        client.commands.get('forage').execute(message, args)
        return
      case 'poll':
        client.commands.get('poll').execute(message, args, client)
        return
      case 'role':
        client.commands.get('role').execute(message, args, client)
        return
      case 'say':
        client.commands.get('say').execute(message, args, client)
        return
      case 'help':
        client.commands.get('help').execute(message, args, avatar)
        return
    }
    // const cmd = {
    //   caw: client.commands.get('caw').execute(message, args, avatar),
    //   crowball: client.commands.get('8ball').execute(message, args),
    //   weather: client.commands.get('weather').execute(message, args),
    //   forage: client.commands.get('forage').execute(message, args),
    //   client: commands.get('poll').execute(message, args, client),
    //   help: client.commands.get('help').execute(message, args, avatar)
    // }
  }

  // Try to see if this bot is mentioned
  let targetMember = message.mentions.members.first()
  if (!targetMember) return
  // If it's not a command, but this bot is mentioned
  if (targetMember.user.id == client.user.id) {
    const args = message.content.slice(prefix.length).split(/ +/)

    if (args.length == 0) return
    for (arg of args) {
      if (arg.includes(targetMember.user.id)) {
        args.splice(args.indexOf(arg), 1)
      }
    }
    const command = args.join(' ')
    switch (command) {
      case '':
        client.commands.get('grumpy').execute(message, args)
        return
      case 'insult me':
        client.commands.get('insult').execute(message, args)
        return
    }
  }
})

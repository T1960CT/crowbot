# CrowBot

## Installation Notes

`npm install`

**COPY** "example.config.json" as "config.json", and enter your Discord App API Key. 
Copying prevents gitignore from making the file disappear on the next commit. 

`node .`

Sample Service using systemd - `/etc/systemd/system/crowbot.service`

```
[Unit]
Description=CrowBot for Discord
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=root
WorkingDirectory=/root/crowbot/crowbot
ExecStart=/root/.nvm/versions/node/v14.11.0/bin/node /root/crowbot/crowbot

[Install]
WantedBy=multi-user.target
```

You can now run

- `systemctl enable crowbot.service`
  - Ensure service is started at system boot
- `systemctl start crowbot.service`
  - Manually start the service
- `systemctl status crowbot.service`
  - See the status of the service, including console output

## Author's Notes

Not currently accepting pull requests

## To Do

- Welcome Messages
- Chat rankings
- Music Player
- Reaction Based Polls (pollbot)
- Interact with CrowBot (feed, pet, gift)
- Stocks/Crypto Ticker
- Foraging Calendar
- Reaction Role Self Assignment (Nero)
- Milestone Role Assignment (Nero)
- Randomly Selected User Fav Role Assignment
- Random Contributions to Conversations
- Caps/Spam Relax
- Memory Persistence?
